# Echo Debug Gen 3
![3D model](images/Echo_Debug_Gen3_3D.png)

This is an adapter board for the Amazon Echo Gen 3 that has a hidden USB port on
the device.

> Same can be achived also with a regular USB cable that's cut and soldered
> correctly to the pads/headers but it's not neat solution.

The device can be booted into fastboot as it runs special locked down version of
AOSP, probably Amazon FireOS.

Since the SoC is Mediatek it should be possible to run an exploit in order to
get execution on the device.

![PCB](images/Echo_Debug_Gen3_PCB.png)

## USB enumeration
If you attach the board correctly you should see the following message in the
kernel logs:
```
[177893.341711] usb 1-7: new high-speed USB device number 31 using xhci_hcd
[1877893.490399] usb 1-7: New USB device found, idVendor=0bb4, idProduct=0c01, bcdDevice= 1.00
[1877893.490405] usb 1-7: New USB device strings: Mfr=1, Product=2, SerialNumber=3
[1877893.490407] usb 1-7: Product: Android
[1877893.490408] usb 1-7: Manufacturer: MediaTek
[1877893.490410] usb 1-7: SerialNumber: G090XG12226401T9
[1877905.969318] usb 1-7: USB disconnect, device number 31
[1877908.877403] usb 1-7: new high-speed USB device number 32 using xhci_hcd
[1877909.026462] usb 1-7: New USB device found, idVendor=0e8d, idProduct=2000, bcdDevice= 1.00
[1877909.026469] usb 1-7: New USB device strings: Mfr=1, Product=2, SerialNumber=0
[1877909.026471] usb 1-7: Product: MT65xx Preloader
[1877909.026473] usb 1-7: Manufacturer: MediaTek
[1877909.109465] cdc_acm 1-7:1.0: Zero length descriptor references
[1877909.109472] cdc_acm: probe of 1-7:1.0 failed with error -22
[1877909.130350] cdc_acm 1-7:1.1: ttyACM0: USB ACM device
[1877909.130378] usbcore: registered new interface driver cdc_acm
[1877909.130380] cdc_acm: USB Abstract Control Model driver for USB modems and ISDN adapters
[1877911.678325] usb 1-7: USB disconnect, device number 32
[1877914.461299] usb 1-7: new high-speed USB device number 33 using xhci_hcd
[1877914.609858] usb 1-7: New USB device found, idVendor=0bb4, idProduct=0c01, bcdDevice= 1.00
[1877914.609864] usb 1-7: New USB device strings: Mfr=1, Product=2, SerialNumber=3
[1877914.609867] usb 1-7: Product: Android
[1877914.609868] usb 1-7: Manufacturer: MediaTek
[1877914.609870] usb 1-7: SerialNumber: G090XG12226401T9
```

## Fastboot
In order to boot into the fastboot mode hold the circle button.
```
$ fastboot getvar all
(bootloader) 	antirback_tee_version: 0x0001
(bootloader) 	antirback_lk_version: 0x0001
(bootloader) 	antirback_pl_version: 0x0001
(bootloader) 	lk_build_desc: 8f885ae-20210414_064357
(bootloader) 	pl_build_desc: eb725ec-20210326_040236
(bootloader) 	secure: yes
(bootloader) 	prod: 1
(bootloader) 	otu_code: m5VzjMfJEV+sXOYzwvb/RA2j0D6VtmZu
(bootloader) 	unlock_status: false
(bootloader) 	unlock_code: 0x54303c50e2e4cb47
(bootloader) 	serialno: G090XG12226401T9
(bootloader) 	max-download-size: 0x8000000
(bootloader) 	kernel: lk
(bootloader) 	product: CRUMPET
(bootloader) 	version-preloader: 0.1.00
(bootloader) 	version: 0.5
all: Done!!
finished. total time: 0.022s
```

## BOM
The Bill of Materials can be found [here](BOM/Echo.csv).

## Changelog
### v1.1
- Fixed the incorrect connection for USB differential pair (D- and D+ swapped)
- Removed the zero ohm resistors
- Added Open Hardware and Alexa logo
- Added optional LEDs for device volatages (+12V and +1.9V)
- Rounded corners of the board
- Sorted pin headers for the voltages
- Replaced USB connector for low cost variant
